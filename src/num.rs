use crate::{op::MathOperation, set::Applyable};

use num_integer::Roots;
use num_rational::Ratio;
use num_traits::{PrimInt, Zero};

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Num {
    Rational(Ratio<i128>),
    Irrational,
}

fn ratio_pow<I: PrimInt + Roots>(a: Ratio<I>, b: Ratio<u32>) -> Option<Ratio<I>> {
    let (an, ad) = a.into();
    let (bn, bd) = b.into();

    let pown = an.pow(bn);
    let powd = ad.pow(bn);

    let n = pown.nth_root(bd);
    let d = powd.nth_root(bd);

    if n.pow(bd) != pown || d.pow(bd) != powd {
        None
    } else {
        Some(Ratio::new(n, d))
    }
}

impl Applyable<MathOperation> for Num {
    fn apply(op: MathOperation, a: Self, b: Self) -> Self {
        use MathOperation::*;
        use Num::*;
        match (a, b) {
            (Irrational, _) => Irrational,
            (_, Irrational) => Irrational,
            (Rational(a), Rational(b)) => match op {
                Add => Rational(a + b),
                Sub => Rational(a - b),
                Mul => Rational(a * b),
                Div => {
                    if b.is_zero() {
                        Irrational
                    } else {
                        Rational(a / b)
                    }
                }
                Pow => {
                    let (bn, bd) = b.into();
                    if let Some(ratio) = ratio_pow(a, Ratio::new(bn as u32, bd as u32)) {
                        Rational(ratio)
                    } else {
                        Irrational
                    }
                }
                Root => {
                    if b.is_zero() {
                        Irrational
                    } else {
                        let (bd, bn) = b.into();
                        if let Some(ratio) = ratio_pow(a, Ratio::new(bn as u32, bd as u32)) {
                            Rational(ratio)
                        } else {
                            Irrational
                        }
                    }
                }
            },
        }
    }
}
