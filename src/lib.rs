mod num;
mod op;
mod set;

pub use num::Num;
pub use op::{MathOperation, Operation, SetOperation};
pub use set::ObjectSet;
use set::{Applyable, SetApplyable};

use tyr_core::{Modifier, Relation, Specifier};

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum TyrItem {
    Unknown,
    Number(Num),
}

impl SetApplyable<Operation> for TyrItem {
    fn set_apply(op: Operation, a: Self, b: Self) -> ObjectSet<Self> {
        use Operation::*;
        use TyrItem::*;
        match op {
            Math(op) => ObjectSet::new(match (a, b) {
                (Unknown, _) => Unknown,
                (_, Unknown) => Unknown,
                (Number(a), Number(b)) => Number(op.apply(a, b)),
            }),
            Set(op) => {
                use SetOperation::*;
                match op {
                    Union => [a, b].into(),
                    Intersection => {
                        if a == b {
                            ObjectSet::new(a)
                        } else {
                            ObjectSet::empty()
                        }
                    }
                }
            }
        }
    }
}

impl Operation {
    pub fn specifier<A: Applyable<Operation>>(self, supplied: A) -> OperationSpecifier<A> {
        OperationSpecifier {
            op: self,
            order: Order::Defaut,
            supplied,
        }
    }

    pub fn relation(self) -> TyrRelation {
        TyrRelation {
            op: self,
            order: Order::Defaut,
        }
    }
}

#[derive(Copy, Clone)]
pub enum Order {
    Defaut,
    Reverse,
}

impl Order {
    pub fn passive(self) -> Self {
        use Order::*;
        match self {
            Defaut => Reverse,
            Reverse => Defaut,
        }
    }
}

#[derive(Clone)]
pub struct OperationSpecifier<O> {
    op: Operation,
    order: Order,
    supplied: O,
}

impl<A: Applyable<Operation>> Specifier<A> for OperationSpecifier<A> {
    fn specify(self, object: A) -> A {
        let Self {
            op,
            order,
            supplied,
        } = self;

        use Order::*;
        let (a, b) = match order {
            Defaut => (object, supplied),
            Reverse => (supplied, object),
        };

        op.apply(a, b)
    }
}

impl<O> OperationSpecifier<O> {
    pub fn opposite(self) -> Self {
        Self {
            op: self.op.opposite(),
            ..self
        }
    }

    pub fn passive(self) -> Self {
        Self {
            order: self.order.passive(),
            ..self
        }
    }
}

#[derive(Clone)]
pub struct TyrRelation {
    op: Operation,
    order: Order,
}

impl<A: Applyable<Operation>> Relation<A, OperationSpecifier<A>> for TyrRelation {
    fn relate(self, supplied: A) -> OperationSpecifier<A> {
        let TyrRelation { op, order } = self;
        OperationSpecifier {
            op,
            order,
            supplied,
        }
    }
}

impl TyrRelation {
    pub fn opposite(self) -> Self {
        Self {
            op: self.op.opposite(),
            ..self
        }
    }

    pub fn passive(self) -> Self {
        Self {
            order: self.order.passive(),
            ..self
        }
    }
}

#[derive(Clone)]
pub enum TyrModifier {
    Opposite,
    Passive,
}

impl<O> Modifier<OperationSpecifier<O>> for TyrModifier {
    fn modify(self, specifier: OperationSpecifier<O>) -> OperationSpecifier<O> {
        use TyrModifier::*;
        match self {
            Opposite => specifier.opposite(),
            Passive => specifier.passive(),
        }
    }
}

impl Modifier<TyrRelation> for TyrModifier {
    fn modify(self, relation: TyrRelation) -> TyrRelation {
        use TyrModifier::*;
        match self {
            Opposite => relation.opposite(),
            Passive => relation.passive(),
        }
    }
}

pub type TyrObject = ObjectSet<TyrItem>;
pub type TyrSpecifier = OperationSpecifier<TyrObject>;
