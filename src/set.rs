use std::collections::BTreeSet;

pub trait Applyable<Op>: Sized {
    fn apply(op: Op, a: Self, b: Self) -> Self;
}

#[derive(Clone)]
pub struct ObjectSet<T>(pub BTreeSet<T>);

impl<T: Ord> ObjectSet<T> {
    pub fn empty() -> Self {
        Self(BTreeSet::new())
    }

    pub fn new(object: T) -> Self {
        let mut set = BTreeSet::new();
        set.insert(object);
        Self(set)
    }

    pub fn add(&mut self, other: Self) {
        self.0.extend(other.0)
    }
}

impl<T: Ord, const N: usize> From<[T; N]> for ObjectSet<T> {
    fn from(objects: [T; N]) -> Self {
        let mut set = BTreeSet::new();
        for object in objects {
            set.insert(object);
        }
        Self(set)
    }
}

pub trait SetApplyable<Op>: Sized {
    fn set_apply(op: Op, a: Self, b: Self) -> ObjectSet<Self>;
}

impl<Op: Copy, A: Clone + SetApplyable<Op> + Ord> Applyable<Op> for ObjectSet<A> {
    fn apply(op: Op, a: Self, b: Self) -> Self {
        let Self(a) = a;
        let Self(b) = b;
        let mut result = ObjectSet::empty();
        for a in a {
            for b in &b {
                result.add(A::set_apply(op, a.clone(), b.clone()));
            }
        }
        result
    }
}
