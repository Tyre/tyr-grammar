use crate::set::{Applyable, ObjectSet, SetApplyable};

#[derive(Copy, Clone)]
pub enum MathOperation {
    Add,
    Sub,
    Mul,
    Div,
    Pow,
    Root,
}

impl MathOperation {
    pub fn apply<A: Applyable<Self>>(self, a: A, b: A) -> A {
        A::apply(self, a, b)
    }

    pub fn set_apply<A: SetApplyable<Self>>(self, a: A, b: A) -> ObjectSet<A> {
        A::set_apply(self, a, b)
    }

    pub fn opposite(self) -> Self {
        use MathOperation::*;
        match self {
            Add => Sub,
            Sub => Add,
            Mul => Div,
            Div => Mul,
            Pow => Root,
            Root => Pow,
        }
    }
}

#[derive(Copy, Clone)]
pub enum SetOperation {
    Union,
    Intersection,
}

impl SetOperation {
    pub fn apply<A: Applyable<Self>>(self, a: A, b: A) -> A {
        A::apply(self, a, b)
    }

    pub fn set_apply<A: SetApplyable<Self>>(self, a: A, b: A) -> ObjectSet<A> {
        A::set_apply(self, a, b)
    }

    pub fn opposite(self) -> Self {
        use SetOperation::*;
        match self {
            Union => Intersection,
            Intersection => Union,
        }
    }
}

#[derive(Copy, Clone)]
pub enum Operation {
    Math(MathOperation),
    Set(SetOperation),
}

impl Operation {
    pub fn apply<A: Applyable<Self>>(self, a: A, b: A) -> A {
        A::apply(self, a, b)
    }

    pub fn set_apply<A: SetApplyable<Self>>(self, a: A, b: A) -> ObjectSet<A> {
        A::set_apply(self, a, b)
    }

    pub fn opposite(self) -> Self {
        use Operation::*;
        match self {
            Math(op) => Math(op.opposite()),
            Set(op) => Set(op.opposite()),
        }
    }
}

impl From<MathOperation> for Operation {
    fn from(op: MathOperation) -> Self {
        Self::Math(op)
    }
}

impl From<SetOperation> for Operation {
    fn from(op: SetOperation) -> Self {
        Self::Set(op)
    }
}
